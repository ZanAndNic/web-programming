window.onload = function(){
    function validate(){
        //Проверка фамилии
        validate_output(/^[a-zA-Zа-яА-Я0-9-_\.]{1,}$/,
                        document.getElementById("SurName").value, "SurNameSpan", "Введите фамилию! Не менее 1 символа");
        //Проверка имени
		validate_output(/^[a-zA-Zа-яА-Я0-9-_\.]{1,}$/,
                        document.getElementById("NameUser").value, "UNameSpan", "Введите имя! Не менее 1 символа");
        //Проверка телефона
		validate_output(/^((8|\+7)(\-| )?(\()?)?(\d){2,3}(\))?(\-| )?(\d){2,3}(\-| )?((\d){2}(\-| )?)?((\d){2}(\-| )?)?$/, 
                        document.getElementById("phone").value, "PhoneSpan", "Введите телефон! Не менее 6 символов");
        //Проверка возраста
		validate_output(/^((\d){1})((((\d){1})?)|([0-4]{1}(\d){1}))$/,
                        document.getElementById("UserAge").value, "UserAgeSpan", "Введите возраст!");
        //Проверка e-mail
		validate_output(/^[a-zA-Zа-яА-Я0-9-_\.]+@[a-zA-Zа-яА-Я0-9-_\.]+\.[A-z]{2,4}$/,
                        document.getElementById("UserEmail").value, "emailSpan", "Введите Email! Email содержит @");
        //Проверка пароля
        validate_password_2();
    }
    function validate_password_2(){
        p1 = validate_output(/^[A-zА-я0-9\W]{8,30}$/,
                        document.getElementById("Password").value, "PasswordSpan", "Введите пароль! От 8 до 30 символов!");
        p2 = validate_output(/^[A-zА-я0-9\W]{8,30}$/,
                        document.getElementById("TwoPassword").value, "TwoPasswordSpan", "Подтвердите пароль!");
        if(p1 == true && p2 == true){
            if(document.getElementById("Password").value != document.getElementById("TwoPassword").value){
                document.getElementById("TwoPasswordSpan").innerHTML = "Пароли не совпадаюь!";
            } 
        }
    }
    //Функция проверки
    function validate_output(reg, str, id, message){
        if(!reg.test(str)){
			document.getElementById(id).innerHTML = message;
            return false;
        }
		else{
			document.getElementById(id).innerHTML = "";
            return true;
		}
    }
    document.getElementById("button_id").onclick = validate;
}