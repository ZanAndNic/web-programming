﻿window.onload = function(){
	var flag = 0;
	function validate_output(reg, str, id, message){
        if(!reg.test(str)){
			document.getElementById(id).innerHTML = message;
            return false;
        }
		else{
			document.getElementById(id).innerHTML = "";
            return true;
		}
	}
	function validate(EventYear,EventDay,EventHours,EventMinutes, NowData,EventData){
		if(validate_output(/^(\d){4}$/, EventYear, "error", "Некорректно введен год")==false){
			return false;
		}
		if(validate_output(/^(((\d){0,2}))$/, EventDay, "error", "Некорректно введен день")==false){
			return false;
		}
		if(validate_output(/^(((\d){0,2}))$/, EventHours, "error", "Некорректно введен час")==false){
			return false;
		}
		if(validate_output(/^(((\d){0,2}))$/, EventMinutes, "error", "Некорректно введен минута")==false){
			return false;
		}
		
		if((EventData-NowData)<=0){
			document.getElementById("error").innerHTML = "Введенная дата меньше текущей!";
			return false;
		}
		return true;
	}
	function GetData(){
		var EventYear = document.getElementById("year").value,
	        EventMonth = document.getElementById("month").value,
	        EventDay = document.getElementById("day").value,
	        EventHours = document.getElementById("hours").value,
	        EventMinutes = document.getElementById("minutes").value;
	
		var NowData = new Date(),
		    EventData = new Date(EventYear, EventMonth, EventDay, EventHours, EventMinutes, 0);
		
		if(validate(EventYear,EventDay,EventHours,EventMinutes, NowData,EventData)==true){
			document.getElementById("OutputNewData").innerHTML = "До события осталось:";
		    timer((EventData - NowData)/1000);
		}
	}
	function timer(sec){
		flag=1;
		if(sec == 10){
			soundClick('muz.mp3');
		}
		if(sec==-1){
			soundClick('toot.mp3');
			return 0;
		}
		var AllTime = sec;
		
		var Day = parseInt( Math.floor(AllTime/60/60/24));
		if(Day < 1){ Day = 0;}
		AllTime = parseInt(AllTime - (Day *60*60*24));
		if(Day < 10){ Day = '0'+Day;}
		
		var Hour = parseInt( Math.floor(AllTime/60/60));
		if(Hour < 1){ Hour = 0;}
		AllTime = parseInt(AllTime - (Hour *60*60));
		if(Hour < 10){ Hour = '0'+Hour;}
		
		var Minutes = parseInt( Math.floor(AllTime/60));
		if(Minutes < 1){ Minutes = 0;}
		AllTime = parseInt(AllTime - (Minutes *60));
		if(Minutes < 10){ Minutes = '0'+Minutes;}
		
		var Seconds = AllTime;
		if(Seconds < 10){ Seconds = '0'+Seconds;}
		
		document.getElementById("OutputDay").innerHTML = Day;
		document.getElementById("OutputHours").innerHTML = Hour;
		document.getElementById("OutputMinutes").innerHTML = Minutes;
		document.getElementById("OutputSeconds").innerHTML = Seconds;
		
		sec--;
		setTimeout(function(){timer( Math.floor(sec));},1000);
	}
	function soundClick(src) {
        var audio = new Audio(); // Создаём новый элемент Audio
        audio.src = src // Указываем путь к звуку "клика"
        audio.autoplay = true; // Автоматически запускаем
	}
	if(flag==0){
	    document.getElementById("start").onclick = GetData;
	}
}