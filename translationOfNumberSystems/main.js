window.onload = function(){
    var getNumber;
    var getOriginalRadix;
   
    function getNumderAndOriginalRadix(id, value){
        if((!!document.getElementById(id).value) == true){
            window.getNumber = document.getElementById(id).value;
            window.getOriginalRadix = value;
        }
    }
	
    function getInputNumbers(){
        getNumderAndOriginalRadix("oneRadix", 2);
        getNumderAndOriginalRadix("twoRadix", 8);
        getNumderAndOriginalRadix("threeRadix", 10);
        getNumderAndOriginalRadix("fourRadix", 16);
        return 0;
    }
	
    function TranslationOfNumbers(){
        getInputNumbers();
        var numberInOriginalRadix = parseInt(window.getNumber, window.getOriginalRadix);
        var resultOfCheck = validation(window.getNumber, window.getOriginalRadix);

        if(resultOfCheck){
            switch(window.getOriginalRadix){
                case 2:
				    outputResults(numberInOriginalRadix, 8, 10, 16);
                    break;
                    
                case 8:
				    outputResults(numberInOriginalRadix, 2, 10, 16);
                    break;
                    
                case 10:
				    outputResults(numberInOriginalRadix, 2, 8, 16);
                    break;
                    
                case 16:
				    outputResults(numberInOriginalRadix, 2, 8, 10);
                    break;
            }
        }
        else{
            сleaningOfFormFields();
        }
    }
 
    function validation(getNumber, getOriginalRadix){
		var flag;
        switch(getOriginalRadix){
            case 2:
				flag = checkGetNumber(getNumber, /[А-Яа-яA-Za-z2-9_]/, "Двоичное число может содержать только цифры 0 и 1");
                break;
                
            case 8:
			    flag = checkGetNumber(getNumber, /[А-Яа-яA-Za-z8-9\W_]/, "Восьмеричное число может содержать только цифры 0 - 7");
                break;
                
            case 10:
			    flag = checkGetNumber(getNumber, /[А-Яа-яA-Za-z\W_]/, "Десятичное число может содержать только цифры 0 - 9");
                break;
                
            case 16:
			    flag = checkGetNumber(getNumber, /[А-Яа-яG-Zg-z\W_]/, "Шестнадцатеричное число может содержать только цифры 0-9, буквы A,B,C,D,E,F");
                break;
            default:
                alert("Что пошло не так! Приносим свои извинения");
                return flag = false;
                break;
        }
		return flag;
    }
    function checkGetNumber(getNumber, regularExpression, message){
		if(regularExpression.test(getNumber)){
            alert(message);
            return false;
        }
		return true;
		
	}
    function сleaningOfFormFields(){
        document.forms["form"].reset();
    }
	function outputResults(numberInOriginalRadix, radixOne, radixTwo, radixThree){
		if(radixOne == 8 && radixTwo == 10 && radixThree == 16){
			document.form.eight.value=(numberInOriginalRadix.toString(8));
            document.form.ten.value=(numberInOriginalRadix.toString(10));
            document.form.hex.value=(numberInOriginalRadix.toString(16).toUpperCase());
		}
		if(radixOne == 2 && radixTwo == 10 && radixThree == 16){
			document.form.two.value=(numberInOriginalRadix.toString(2));
            document.form.ten.value=(numberInOriginalRadix.toString(10));
            document.form.hex.value=(numberInOriginalRadix.toString(16).toUpperCase());
		}
		if(radixOne == 2 && radixTwo == 8 && radixThree == 16){
			document.form.two.value=(numberInOriginalRadix.toString(2));
            document.form.eight.value=(numberInOriginalRadix.toString(8));
            document.form.hex.value=(numberInOriginalRadix.toString(16).toUpperCase());
		}
		if(radixOne == 2 && radixTwo == 8 && radixThree == 10){
			document.form.two.value=(numberInOriginalRadix.toString(2));
            document.form.eight.value=(numberInOriginalRadix.toString(8));
            document.form.ten.value=(numberInOriginalRadix.toString(10));
		}
	}
	
    document.getElementById("oneRadix").onchange = TranslationOfNumbers;
    document.getElementById("twoRadix").onchange = TranslationOfNumbers;
    document.getElementById("threeRadix").onchange = TranslationOfNumbers;
    document.getElementById("fourRadix").onchange = TranslationOfNumbers;
    
    document.getElementById("actionButton").onclick = сleaningOfFormFields;
}